#!/bin/bash

function createJobs {
  SAVEIFS=$IFS
  IFS=$(echo -en "\n\b")
  
  for x in `find . -type f -name "service-*.xml"`; do
    JOB_NAME=$(echo $x | sed -e 's/.*\///' | cut -d "." -f 1 | sed -e 's/-config//')
    echo Creating job: ${JOB_NAME}
    cat $x | curl -X POST http://localhost:8080/createItem?name=${JOB_NAME} --data-binary "@-" -H "Content-Type: text/xml"
  done

  IFS=$SAVEIFS
}

echo Create Jobs:
createJobs
